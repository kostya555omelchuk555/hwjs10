const tabs = document.querySelectorAll('.tabs-title');
const content = document.querySelectorAll('.tabs-content li');

tabs.forEach(function(tab2, i) {
  tab2.addEventListener('click', () => {
    // Відключаємо активний клас для всіх вкладок
    tabs.forEach(tab2 => tab2.classList.remove('active'));
    // Додаємо активний клас до поточної вкладки
    tab2.classList.add('active');
    // Ховаємо весь контент
    content.forEach(item => item.style.display = 'none');
    // Показуємо потрібний контент
    content[i].style.display = 'block';

	  	  console.log(i)
  });
});
 content.forEach(item => item.style.display = 'none');
   content[0].style.display = 'block';